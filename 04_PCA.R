# Packages ----------------------------------------------------------------
library(Rplot.extra)
library(grid)
library(ggplot2)
library(massageR)
library(dplyr)
library(magrittr)
library(tidyr)



# Figure out how many plots to make ---------------------------------------
plot_group <- data %>% 
  select(tissue,group) %>% 
  distinct %>% 
  mutate_each(funs(as.character)) %>% 
  as.data.frame



# Make all the plots ------------------------------------------------------
for(i in 0:nrow(plot_group)){
  
  
  # Remove V. champinii in 2007. It is crazy. See heatmaps.
  data_select <- data %>% filter(!(species == "V. champinii" & year == "2007" & tissue == "powder" & group == "Lipids"))
  
  
  # Select only the right data and match to row color
  data_select %<>% 
                 mutate_each(funs(as.character),tissue,species,year,compounds,group) %>% 
                 unite(label,species,year, sep = " - ",remove = FALSE)
    
    if(i==0){
      data_select %<>% mutate(compounds = paste0(compounds," (",group,")"))
    }else{
      data_select %<>% filter(tissue == plot_group[i,"tissue"] & group == plot_group[i,"group"])
    }              
  
  
  
  data_select %<>% 
                    select(-tissue,-group) %>% 
                    spread(compounds,value) %>% 
                    left_join(color_species,by="species")
  
  
  # get rowcolor
  rowcolor <- data_select %>% select(color) %>% as.matrix %>% as.character
  
  # set rownames
  rownames(data_select) <- data_select %>% select(label) %>% as.matrix %>% as.character
  
  # get species
  species <- data_select %>% select(species) %>% as.matrix %>% as.character
  
  # remove non-data columns
  data_select %<>% select(-label,-species,-year,-color)
  
  
  # Remove columns with >50% NA
  data_select %<>%  as.data.frame %>% 
    select(   which(sapply(.,function(x) sum(!is.na(x))>(length(x)*0.5)   ))   ) %>% 
    as.matrix
  
  
  # remove "% Diglucosidi"
  rem_col <- c("Diglucosides","diglucosides","galloylation", "mDP") %>% 
    paste(collapse="|")
  
  data_select %<>% as.data.frame %>% select(grep(rem_col,colnames(.), invert = TRUE, ignore.case = TRUE)) %>% as.matrix
  
  
  p <- PCA_biplot(data_select,
                  # The PCA calculation
                  pcs = c(1,2),
                  scale = "uv",
                  method="nipals",
                  
                  # Plotting
                  foreground="scores",
                  
                  fg_geom = "lines",
                  fg_color= rowcolor,
                  fg_class = species,
                  fg_labels = rownames(data_select),
                  
                  bg_geom = "dots",
                  bg_color = "grey",
                  bg_class = "",
                  bg_labels = colnames(data_select),
                  
                  # Fine tune plot
                  loading_quantile = 0.99,
                  text_offset=0.02
  )
  
  
  p$layers[[2]]$aes_params$size = 5   # loadings text, was 3
  p$layers[[1]]$aes_params$size = 6   # loadings dots, was 4
  p$layers[[4]]$aes_params$size = 7  # scores text, was 5
  
  
  p <- p +  theme(legend.position = "right") +
            theme(legend.title = element_text(size=18, face="bold")) + # Title appearance
            theme(legend.text = element_text(size = 18)) +             # Label appearance
            theme(legend.key.size = unit(1.5, 'lines')) +
            theme(axis.title.x = element_text(face="bold", size=20), axis.title.y = element_text(face="bold", size=20)) +
            theme(legend.key = element_blank())
  
  
  gt <- ggplot_gtable(ggplot_build(p + scale_x_continuous(expand=c(0.3,0.3))))
  gt$layout$clip[gt$layout$name == "panel"] <- "off"
  
  
  
  if(i==0){
    png(file="_plots/PCA_all.png",width=6000*0.9,height=4500*0.9, res = 300)
    par(oma = c(30, 5, 0, 18),xpd=TRUE)
  }else{
    png(file=paste0("_plots/","PCA_",plot_group[i,"tissue"],"_",plot_group[i,"group"],".png"),width=6000*0.9,height=4500*0.9, res = 300)
    par(oma = c(30, 5, 0, 18),xpd=TRUE)
  } 
  
  
  grid.draw(gt)
  dev.off()
  
  
  
  
  if(i==0){
    pdf(file="_plots/PCA_all.pdf",width=(6000/72)*(72/300)*0.9,height=(4500/72)*(72/300)*0.9, onefile = TRUE, useDingbats = FALSE)
    par(oma = c(30, 5, 0, 18),xpd=TRUE)
  }else{
    pdf(file=paste0("_plots/","PCA_",plot_group[i,"tissue"],"_",plot_group[i,"group"],".pdf"),width=(6000/72)*(72/300)*0.9,height=(4500/72)*(72/300)*0.9, onefile = TRUE, useDingbats = FALSE)
    par(oma = c(30, 5, 0, 18),xpd=TRUE)
  } 
  
  
  grid.draw(gt)
  dev.off()  
  
  

}



# Clean-up ----------------------------------------------------------------
rm(data_select,i,rowcolor,p,plot_group,gt,species, rem_col)


