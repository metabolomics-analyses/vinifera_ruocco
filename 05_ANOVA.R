# Packages ----------------------------------------------------------------
library(multcomp)
library(dplyr)
library(multidplyr)
library(tidyr)
library(parallel)
library(purrr)
library(broom)
library(massageR)
library(fdrtool)
library(multtest)
library(xlsx)


# Helper functions --------------------------------------------------------
TSBH_q <- function(x) {
  require(multtest)
  res <- mt.rawp2adjp(x, "TSBH")
  res <- res$adjp[order(res$index), 2]
  return(res) 
}


p.value_add.correction <- function(x){
  require(dplyr)
  require(fdrtool)
  
  out <- x %>% ungroup %>%
    mutate(   q.value.tail_area      = fdrtool(p.value,statistic = "pvalue", plot = FALSE)$qval   ,
              q.value.lfdr           = fdrtool(p.value,statistic = "pvalue", plot = FALSE)$lfdr   ,
              q.value.TSBH           = TSBH_q(p.value)                              ) 
  
  return(out)
}




# Find varieties with meassurements in 2007 to 2009 -----------------------
data %>%  
          group_by(tissue,species,year,group) %>% 
          summarise(count = ifelse(length(value)>0,TRUE,FALSE)) %>%
          ungroup %>%
          spread(year,count,fill=FALSE) %>% 
          arrange(tissue,group,species) %>% 
          mutate(select= ifelse(`2007`==TRUE & `2008`==TRUE & `2009`==TRUE,TRUE,FALSE)) %>% 
          filter(select) ->
select_varieties


data_select <-  data %>% 
                rowwise %>% 
                filter(any(tissue==select_varieties$tissue & species==select_varieties$species & group==select_varieties$group)) %>% 
                ungroup %>% 
                filter(year %in% as.character(2007:2009))



# Replace NAs -------------------------------------------------------------
data_select %<>%  group_by(tissue,group,compounds) %>% 
                  mutate(value = rep_na(value %>% as.data.frame,rep.FUN = rand.to.min) %>% as.numeric) %>% 
                  ungroup



# Do ANOVA ----------------------------------------------------------------
lm_fw      <- failwith(NULL,lm     ,quiet=TRUE)
glht_fw    <- failwith(NULL,glht   ,quiet=TRUE)
summary_fw <- failwith(NULL,summary,quiet=TRUE)


cluster <- create_cluster(2) %>% cluster_library("purrr") %>% cluster_library("multcomp")
clusterExport(cluster,c("lm_fw","glht_fw","summary_fw"))


stats <-  data_select %>% 
          group_by(tissue, group, compounds) %>% nest %>% 
          partition(cluster = cluster, tissue, group, compounds) %>% 
          mutate(model         = map(data,  ~ lm_fw(value ~ species, data = .))          ) %>% 
          mutate(multicomp     = map(model, ~ glht_fw(.,linfct = mcp(species = "Tukey")))  ) %>% 
          mutate(multicomp_sum = map(multicomp, ~ summary_fw(.,test=adjusted("none")))  ) %>% 
          collect()

stopCluster(cluster)


stats_tidy <- stats %>%
              ungroup %>%
              rowwise %>% 
              filter(!is.null(multicomp)) %>%
              ungroup %>%
              unnest(multicomp_sum %>% map(tidy)) %>% 
              p.value_add.correction


stats_tidy_wide <- stats_tidy %>% 
                   select(tissue,group,compounds,lhs,q.value.lfdr) %>% 
                   spread(lhs,q.value.lfdr)


rm(data_select,select_varieties)

dir.create("_ANOVA",showWarnings = FALSE)
write.xlsx(stats_tidy_wide,"_ANOVA/ANOVA.xlsx")
