# vinifera_ruocco

## What is this?
This is the data analysis for the paper:

[Silvia Ruocco, Marco Stefanini, Jan Stanstrup, Daniele Perenzoni, Fulvio Mattivi, Urska Vrhovsek, **The metabolomic profile of red non-V. vinifera genotypes**, Food Research International.](http://dx.doi.org/10.1016/j.foodres.2017.01.024)

## Using this repository

You will need to install [Git Large File Storage (LFS)](https://git-lfs.github.com) to be able download this repository.
To be sure that the scripts are reproducible you will need to use R version 3.3.1.
This repository is a so-called Packrat project. That means that it contains all used R packages and will thus stay reproducible.
